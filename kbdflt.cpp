#include <wdm.h>
#include <ntstrsafe.h>

#include "common.h"
#include "kbdflt.h"
#include "ezusbsys.h"

// private
void IOPollingThread(IN PVOID);
NTSTATUS KFIoctl(IN PDEVICE_OBJECT deviceObject, IN PIRP irp);
NTSTATUS GetHidDescriptor(IN PDEVICE_OBJECT deviceObject, IN PIRP irp);
NTSTATUS GetReportDescriptor(IN PDEVICE_OBJECT deviceObject, IN PIRP irp);
NTSTATUS GetDeviceAttributes(IN PDEVICE_OBJECT deviceObject, IN PIRP irp);

LONG g_runThread = 1;
HANDLE g_threadHandle = 0;
PDEVICE_OBJECT g_devObj = nullptr;
CONNECT_DATA g_connData{};

const UCHAR g_reportDescriptor[] = {
	0x05, 0x01,        // USAGE_PAGE (Generic Desktop)
	0x09, 0x06,        // USAGE (Keyboard)
	0xA1, 0x01,        // COLLECTION (Application)
	0x85,                  REPORT_ID_KEYBOARD_INPUT,
	0x05, 0x07,            // USAGE_PAGE (Keyboard Key Codes)
	0x19, 0xE0,            // USAGE_MINIMUM (224)
	0x29, 0xE7,            // USAGE_MAXIMUM (231)
	0x15, 0x00,            // LOGICAL_MINIMUM (0)
	0x25, 0x01,            // LOGICAL_MAXIMUM (1)
	0x75, 0x01,            // REPORT_SIZE (1)
	0x95, 0x08,            // REPORT_COUNT (8)
	0x81, 0x02,            // INPUT (Data, Variable, Absolute)
	0x95, 0x01,            // REPORT_COUNT (1)
	0x75, 0x08,            // REPORT_SIZE (8)
	0x81, 0x01,            // INPUT (Constant)
	0x19, 0x00,            // USAGE_MINIMUM (0)
	0x29, 0x65,            // USAGE_MAXIMUM (101)
	0x15, 0x00,            // LOGICAL_MINIMUM (0)
	0x25, 0x65,            // LOGICAL_MAXIMUM (101)
	0x95, 0x06,            // REPORT_COUNT (6)
	0x75, 0x08,            // REPORT_SIZE (8)
	0x81, 0x00,            // INPUT (Data, Array, Absolute)
	0x05, 0x08,            // USAGE_PAGE (LEDs)
	0x19, 0x01,            // USAGE_MINIMUM (Num Lock)
	0x29, 0x05,            // USAGE_MAXIMUM (Kana)
	0x95, 0x05,            // REPORT_COUNT (5)
	0x75, 0x01,            // REPORT_SIZE (1)
	0x91, 0x02,            // OUTPUT (Data, Variable, Absolute)
	0x95, 0x01,            // REPORT_COUNT (1)
	0x75, 0x03,            // REPORT_SIZE (3)
	0x91, 0x01,            // OUTPUT (Constant)
	0xC0,              // END_COLLECTION

	0x05, 0x01,        // USAGE_PAGE (Generic Desktop)
	0x09, 0x00,        // USAGE (Undefined)
	0xa1, 0x01,        // COLLECTION (Application)
	0x85,                  REPORT_ID_KEYBOARD_OUTPUT,
	0x09, 0x00,            // USAGE (Undefined)
	0x15, 0x00,            // LOGICAL_MINIMUM (0)
	0x26, 0xff, 0x00,      // LOGICAL_MAXIMUM (255)
	0x95, 0x08,            // REPORT_COUNT (8)
	0x75, 0x08,            // REPORT_SIZE (8)
	0x91, 0x02,            // OUTPUT (Data, Variable, Absolute)
	0xc0               // END_COLLECTION
};

const HID_DESCRIPTOR g_hidDescriptor = {
	0x09,        // length of HID descriptor
	0x21,        // descriptor type == HID 0x21
	0x0100,      // hid spec release
	0x00,        // country code == Not Specified
	0x01,        // number of HID class descriptors
	{            // DescriptorList[0]
		0x22,                             // report descriptor type 0x22
		sizeof(g_reportDescriptor)        // total length of report descriptor
	}
};
// private end

NTSTATUS KFPostDriverEntry(IN PDRIVER_OBJECT driverObject, IN PUNICODE_STRING registryPath) {
	NTSTATUS ntStatus = STATUS_SUCCESS;

	DEBUGPRINT("start");

	driverObject->MajorFunction[IRP_MJ_INTERNAL_DEVICE_CONTROL] = KFIoctl;

	UNREFERENCED_PARAMETER(registryPath);
	DEBUGPRINT("end %d", ntStatus);
	return ntStatus;
}

void KFDeviceInit(IN PDEVICE_OBJECT deviceObject) {
	auto devExt = (PDEVICE_EXTENSION)deviceObject->DeviceExtension;

	DEBUGPRINT("start");

	devExt->HidDescriptor = g_hidDescriptor;
	devExt->ReportDescriptor = g_reportDescriptor;

	PsCreateSystemThread(&g_threadHandle, THREAD_ALL_ACCESS, NULL, 0, NULL, IOPollingThread, devExt);

	_InterlockedCompareExchangePointer((PVOID*)&g_devObj, deviceObject, 0);

	DEBUGPRINT("end");
}

void KFDeviceTini(IN PDEVICE_OBJECT deviceObject) {
	DEBUGPRINT("start");

	_InterlockedCompareExchangePointer((PVOID*)&g_devObj, 0, deviceObject);

	DEBUGPRINT("end");
}

void KFDriverUnload(IN PDRIVER_OBJECT driverObject) {
	DEBUGPRINT("start");

	InterlockedExchange(&g_runThread, 0);

	if (g_threadHandle) {
		PVOID threadObject = nullptr;

		ObReferenceObjectByHandle(g_threadHandle, 0, nullptr, MODE::KernelMode, &threadObject, nullptr);
		KeWaitForSingleObject(threadObject, KWAIT_REASON::Executive, MODE::KernelMode, false, nullptr);
		ObDereferenceObject(threadObject);
		g_threadHandle = 0;
	}

	g_devObj = nullptr;

	UNREFERENCED_PARAMETER(driverObject);

	DEBUGPRINT("end");
}

NTSTATUS KFIoctl(IN PDEVICE_OBJECT deviceObject, IN PIRP irp) {
	auto ntStatus = STATUS_SUCCESS;

	DEBUGPRINT("start");

	auto irpStack = IoGetCurrentIrpStackLocation(irp);
	auto ioControlCode = irpStack->Parameters.DeviceIoControl.IoControlCode;

	switch (ioControlCode)
	{
	case IOCTL_HID_GET_DEVICE_DESCRIPTOR:
		//
		// Retrieves the device's HID descriptor. 
		//
		DEBUGPRINT("IOCTL_HID_GET_DEVICE_DESCRIPTOR");
		ntStatus = GetHidDescriptor(deviceObject, irp);
		break;

	case IOCTL_HID_GET_REPORT_DESCRIPTOR:
		//
		//Obtains the report descriptor for the HID device.
		//
		DEBUGPRINT("IOCTL_HID_GET_REPORT_DESCRIPTOR");
		ntStatus = GetReportDescriptor(deviceObject, irp);
		break;

	case IOCTL_HID_GET_DEVICE_ATTRIBUTES:
		//
		//Retrieves a device's attributes in a HID_DEVICE_ATTRIBUTES structure.
		//
		DEBUGPRINT("IOCTL_HID_GET_DEVICE_ATTRIBUTES");
		ntStatus = GetDeviceAttributes(deviceObject, irp);
		break;

	case IOCTL_HID_GET_INPUT_REPORT:
		DEBUGPRINT("IOCTL_HID_GET_INPUT_REPORT");
		ntStatus = STATUS_NOT_IMPLEMENTED;
		break;

	case IOCTL_INTERNAL_KEYBOARD_CONNECT: {
		DEBUGPRINT("IRP_MJ_INTERNAL_DEVICE_CONTROL / IOCTL_INTERNAL_KEYBOARD_CONNECT");
		if (irpStack->Parameters.DeviceIoControl.InputBufferLength < sizeof(CONNECT_DATA)) {
			ntStatus = STATUS_INVALID_PARAMETER;
			break;
		}

		auto connectData = (PCONNECT_DATA)irpStack->Parameters.DeviceIoControl.Type3InputBuffer;
		g_connData = *connectData;
		break;
	}

	default:
		DEBUGPRINT("unknown code %d", ioControlCode);
		ntStatus = STATUS_NOT_IMPLEMENTED;
		break;
	}


	irp->IoStatus.Status = ntStatus;
	IoCompleteRequest(irp, IO_NO_INCREMENT);
	//ntStatus = STATUS_SUCCESS;

	DEBUGPRINT("end %d", ntStatus);

	// 의도한 것
	return STATUS_SUCCESS;
}

NTSTATUS GetHidDescriptor(IN PDEVICE_OBJECT deviceObject, IN PIRP irp) {
	/*++

	Routine Description:

		Finds the HID descriptor and copies it into the buffer provided by the Irp.

	Arguments:

		DeviceObject - pointer to a device object.

		Irp - Pointer to Interrupt Request Packet.

	Return Value:

		NT status code.

	--*/
	auto ntStatus = STATUS_SUCCESS;

	DEBUGPRINT("start");

	//
	// Get a pointer to the current location in the Irp
	//

	auto irpStack = IoGetCurrentIrpStackLocation(irp);

	//
	// Get a pointer to the device extension
	//

	auto devExt = (PDEVICE_EXTENSION)deviceObject->DeviceExtension;

	//
	// Copy MIN (OutputBufferLength, DeviceExtension->HidDescriptor->bLength)
	//

	auto bytesToCopy = irpStack->Parameters.DeviceIoControl.OutputBufferLength;
	if (!bytesToCopy) {
		return STATUS_BUFFER_TOO_SMALL;
	}

	//
	// Since HidDescriptor.bLength could be >= sizeof (HID_DESCRIPTOR) we 
	// just check for HidDescriptor.bLength and 
	// copy MIN (OutputBufferLength, DeviceExtension->HidDescriptor->bLength)
	//

	if (bytesToCopy > devExt->HidDescriptor.bLength) {
		bytesToCopy = devExt->HidDescriptor.bLength;
	}

	DEBUGPRINT("Copying %d bytes to HIDCLASS buffer\n", bytesToCopy);

	RtlCopyMemory((PUCHAR)irp->UserBuffer, (PUCHAR)&devExt->HidDescriptor, bytesToCopy);

	//
	// Report how many bytes were copied
	//
	irp->IoStatus.Information = bytesToCopy;

	DEBUGPRINT("end %d", ntStatus);

	return ntStatus;
}

NTSTATUS GetReportDescriptor(IN PDEVICE_OBJECT deviceObject, IN PIRP irp) {
	/*++

	Routine Description:

		Finds the Report descriptor and copies it into the buffer provided by the
		Irp.

	Arguments:

		DeviceObject - pointer to a device object.

		Irp - Pointer to Interrupt Request Packet.

	Return Value:

		NT status code.

	--*/

	auto ntStatus = STATUS_SUCCESS;

	DEBUGPRINT("start");

	auto irpStack = IoGetCurrentIrpStackLocation(irp);
	auto devExt = (PDEVICE_EXTENSION)deviceObject->DeviceExtension;

	//
	// Copy device descriptor to HIDCLASS buffer
	//

	DEBUGPRINT("HIDCLASS Buffer = 0x%x, Buffer length = 0x%x\n",
		irp->UserBuffer,
		irpStack->Parameters.DeviceIoControl.OutputBufferLength);

	SIZE_T bytesToCopy = irpStack->Parameters.DeviceIoControl.OutputBufferLength;

	if (!bytesToCopy) {
		return STATUS_BUFFER_TOO_SMALL;
	}

	if (bytesToCopy > devExt->HidDescriptor.DescriptorList[0].wReportLength) {
		bytesToCopy = devExt->HidDescriptor.DescriptorList[0].wReportLength;
	}

	DEBUGPRINT("Copying 0x%x bytes to HIDCLASS buffer\n", bytesToCopy);

	RtlCopyMemory((PUCHAR)irp->UserBuffer, (PUCHAR)devExt->ReportDescriptor, bytesToCopy);

	//
	// Report how many bytes were copied
	//
	irp->IoStatus.Information = bytesToCopy;

	DEBUGPRINT("end %d", ntStatus);

	return ntStatus;

}

NTSTATUS GetDeviceAttributes(IN PDEVICE_OBJECT deviceObject, IN PIRP irp) {
	/*++

	Routine Description:

		Fill in the given struct _HID_DEVICE_ATTRIBUTES

	Arguments:

		DeviceObject - pointer to a device object.

		Irp - Pointer to Interrupt Request Packet.

	Return Value:

		NT status code.

	--*/

	auto ntStatus = STATUS_SUCCESS;

	DEBUGPRINT("start");

	auto irpStack = IoGetCurrentIrpStackLocation(irp);
	auto deviceAttributes = (PHID_DEVICE_ATTRIBUTES)irp->UserBuffer;

	if (sizeof(HID_DEVICE_ATTRIBUTES) > irpStack->Parameters.DeviceIoControl.OutputBufferLength) {
		return STATUS_BUFFER_TOO_SMALL;
	}

	irp->IoStatus.Information = sizeof(HID_DEVICE_ATTRIBUTES);
	deviceAttributes->Size = sizeof(HID_DEVICE_ATTRIBUTES);
	deviceAttributes->VendorID = HIDKBD_VID;
	deviceAttributes->ProductID = HIDKBD_PID;
	deviceAttributes->VersionNumber = HIDKBD_VERSION;

	UNREFERENCED_PARAMETER(deviceObject);

	DEBUGPRINT("end %d", ntStatus);

	return ntStatus;
}

ULONGLONG PullIO(IN PDEVICE_OBJECT deviceObject, OUT PURB urb) {
	ULONGLONG key{};

	auto urbSize = (USHORT)sizeof(struct _URB_CONTROL_VENDOR_OR_CLASS_REQUEST);

	RtlZeroMemory(urb, urbSize);

	urb->UrbHeader.Length = urbSize;
	urb->UrbHeader.Function = URB_FUNCTION_VENDOR_DEVICE;

	urb->UrbControlVendorClassRequest.TransferFlags = USBD_SHORT_TRANSFER_OK | USBD_TRANSFER_DIRECTION_IN;
	urb->UrbControlVendorClassRequest.TransferBufferLength = sizeof(key);
	urb->UrbControlVendorClassRequest.TransferBuffer = &key;
	urb->UrbControlVendorClassRequest.Request = 0xAE;
	urb->UrbControlVendorClassRequest.Value = 0;
	urb->UrbControlVendorClassRequest.Index = 0;

	auto ntStatus = Ezusb_CallUSBD(deviceObject, urb);
	if (!NT_SUCCESS(ntStatus)) {
		DEBUGPRINT("fail %d", ntStatus);
	}

	return key;
}

void PushIO(IN PDEVICE_OBJECT deviceObject, IN ULONGLONG led, OUT PURB urb) {
	auto urbSize = (USHORT)sizeof(struct _URB_CONTROL_VENDOR_OR_CLASS_REQUEST);

	RtlZeroMemory(urb, urbSize);

	urb->UrbHeader.Length = urbSize;
	urb->UrbHeader.Function = URB_FUNCTION_VENDOR_DEVICE;

	urb->UrbControlVendorClassRequest.TransferFlags = USBD_SHORT_TRANSFER_OK;
	urb->UrbControlVendorClassRequest.TransferBufferLength = sizeof(led);
	urb->UrbControlVendorClassRequest.TransferBuffer = &led;
	urb->UrbControlVendorClassRequest.Request = 0xAE;
	urb->UrbControlVendorClassRequest.Value = 0;
	urb->UrbControlVendorClassRequest.Index = 0;

	auto ntStatus = Ezusb_CallUSBD(deviceObject, urb);
	if (!NT_SUCCESS(ntStatus)) {
		DEBUGPRINT("fail %d", ntStatus);
	}
}

void SendKey(IN ULONGLONG prev, IN ULONGLONG cur) {
	// https://github.com/racerxdl/piuio_clone/blob/master/docs/piuio.txt
	static const int ioKeyCodeTable[][2] = {
		{0x00000008, 0x2c}, // 1p left bottom, q
		{0x00000010, 0x2e}, // 1p right bottom, e
		{0x00000004, 0x1f}, // 1p center, s
		{0x00000001, 0x10}, // 1p left top, z
		{0x00000002, 0x12}, // 1p right top, c

		{0x00080000, 0x2f}, // 2p left bottom, r
		{0x00100000, 0x31}, // 2p right bottom, y
		{0x00040000, 0x22}, // 2p center, g
		{0x00010000, 0x13}, // 2p left top, v 
		{0x00020000, 0x15}, // 2p right top, n

		{0x00004000, 0x02}, // service, 1
		{0x00000200, 0x03}, // test, 2
	};
	constexpr int tableSize = sizeof(ioKeyCodeTable) / sizeof(*ioKeyCodeTable);

	KEYBOARD_INPUT_DATA keyData[tableSize];
	RtlZeroMemory(keyData, sizeof(keyData));
	int j = 0;

	// https://download.microsoft.com/download/1/6/1/161ba512-40e2-4cc9-843a-923143f3456c/translate.pdf
	for (int i = 0; i < tableSize; i++) {
		auto io = ioKeyCodeTable[i][0];
		auto key = (USHORT)ioKeyCodeTable[i][1];

		if ((prev & io) != (cur & io)) {
			keyData[j].UnitId = 1;
			keyData[j].MakeCode = key;
			keyData[j].Flags = (cur & io) ? KEY_BREAK : KEY_MAKE;

			j++;
		}
	}

	ULONG comsumed = 0;

	// https://docs.microsoft.com/en-us/previous-versions/ff542324(v=vs.85)
	if (j && g_connData.ClassService) {
		(*(PSERVICE_CALLBACK_ROUTINE)(ULONG_PTR)g_connData.ClassService)(
			g_connData.ClassDeviceObject,
			&keyData,
			keyData + j,
			&comsumed);
	}
}

void IOPollingThread(IN PVOID) {
	/*
	led
	0 right, 1 left, 2 bottom, 3 top
	*/

	DEBUGPRINT("start");

	LARGE_INTEGER tick{};
	ULONGLONG led{};
	ULONGLONG prev = (ULONGLONG)-1LL;
	ULONGLONG cur = prev;

	auto urbSize = sizeof(struct _URB_CONTROL_VENDOR_OR_CLASS_REQUEST);
	auto urb = (PURB)ExAllocatePool(NonPagedPool, urbSize);
	if (!urb) {
		DEBUGPRINT("alloc fail!");
		return;
	}

	// 5ms 
	tick.QuadPart = -1LL * 5 * 10000;

	while (_InterlockedCompareExchange(&g_runThread, 0, 0)) {
		auto deviceObject = (PDEVICE_OBJECT)_InterlockedCompareExchangePointer((PVOID*)&g_devObj, 0, 0);
		if (!deviceObject) {
			KeDelayExecutionThread(MODE::KernelMode, false, &tick);
			continue;
		}

		PushIO(deviceObject, led | 0x0000000000000000ULL, urb);
		auto key1 = PullIO(deviceObject, urb);
		PushIO(deviceObject, led | 0x0000000000010001ULL, urb);
		auto key2 = PullIO(deviceObject, urb);
		PushIO(deviceObject, led | 0x0000000000020002ULL, urb);
		auto key3 = PullIO(deviceObject, urb);
		PushIO(deviceObject, led | 0x0000000000030003ULL, urb);
		auto key4 = PullIO(deviceObject, urb);


		// 하나라도 눌렸으면 눌린거임
		cur = key1 & key2 & key3 & key4;
		SendKey(prev, cur);
		led = ((~cur) & 0x1f001f) << 2;
		prev = cur;

		DEBUGPRINT("tick %08llx %08llx %08llx %08llx", key1, key2, key3, key4);

		KeDelayExecutionThread(MODE::KernelMode, false, &tick);
	}

	ExFreePool(urb);
}
