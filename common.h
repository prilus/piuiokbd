#pragma once

#include <wdm.h>
#include <kbdmou.h>

extern "C" {
#include <usbdi.h>
#include <usbdlib.h>
#include <hidport.h>
}

#include "consts.h"
#include "ringbuffer.h"

#if DBG

#define Ezusb_KdPrint(_x_) DbgPrint("Ezusb.SYS: "); \
                             DbgPrint _x_ ;
#define TRAP() DbgBreakPoint()
#else
#define Ezusb_KdPrint(_x_)
#define TRAP()
#endif

#if _DEBUG
#define DEBUGPRINT(fmt, ...) do { \
		/*RtlStringCbPrintfA(g_debuglog, 0x1000 - 1, "%s: " ## fmt, __func__, __VA_ARGS__);*/ \
		DbgPrint("%s: " ## fmt, __func__, __VA_ARGS__); \
	}while(false)
#else
#define DEBUGPRINT(fmt, ...)
#endif

//
// A structure representing the instance information associated with
// this particular device.
//
typedef struct _DEVICE_EXTENSION {
	// original ezusb device extension

	// physical device object
	PDEVICE_OBJECT PhysicalDeviceObject;

	// Device object we call when submitting Urbs/Irps to the USB stack
	PDEVICE_OBJECT		StackDeviceObject;

	// Indicates that we have recieved a STOP message
	BOOLEAN Stopped;

	// Indicates that we are enumerated and configured.  Used to hold
	// of requests until we are ready for them
	BOOLEAN Started;

	// Indicates the device needs to be cleaned up (ie., some configuration
	// has occurred and needs to be torn down).
	BOOLEAN NeedCleanup;

	// configuration handle for the configuration the
	// device is currently in
	USBD_CONFIGURATION_HANDLE ConfigurationHandle;

	// ptr to the USB device descriptor
	// for this device
	PUSB_DEVICE_DESCRIPTOR DeviceDescriptor;

	// we support up to one interface
	PUSBD_INTERFACE_INFORMATION Interface;

	// the number of device handles currently open to the device object.
	// Gets incremented by Create and decremented by Close
	ULONG                OpenHandles;

	// Name buffer for our named Functional device object link
	WCHAR DeviceLinkNameBuffer[Ezusb_NAME_MAX];

	// This member is used to store the URB status of the
	// most recently failed URB.  If a USB transfer fails, a caller
	// can use IOCTL_EZUSB_GET_LAST_ERROR to retrieve this value.
	// There's only room for one, so you better get it quick (or at
	// least before the next URB failure occurs).
	USBD_STATUS LastFailedUrbStatus;

	// use counter for the device.  Gets incremented when the driver receives
	// a request and gets decremented when a request s completed.
	LONG usage;

	// this ev gets set when it is ok to remove the device
	KEVENT evRemove;

	// TRUE if we're trying to remove this device
	BOOLEAN removing;

	BOOLEAN StopIsoStream;

	PRING_BUFFER DataRingBuffer;
	PRING_BUFFER DescriptorRingBuffer;

	// end original ezusb device extension

	HID_DESCRIPTOR HidDescriptor;
	const UCHAR *ReportDescriptor;
	
} DEVICE_EXTENSION, *PDEVICE_EXTENSION;
