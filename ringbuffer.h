#pragma once
#include <wdm.h>

#ifdef DRIVER

typedef struct _RING_BUFFER
{
	PUCHAR      inPtr;
	PUCHAR      outPtr;
	ULONG       totalSize;
	ULONG       currentSize;
	KSPIN_LOCK	spinLock;
	PUCHAR      buffer;
} RING_BUFFER, *PRING_BUFFER;

PRING_BUFFER
AllocRingBuffer(
	ULONG    Size
);

VOID
FreeRingBuffer(
	PRING_BUFFER   ringBuffer
);

ULONG
ReadRingBuffer(
	PRING_BUFFER   ringBuffer,
	PUCHAR         readBuffer,
	ULONG          numberOfBytesToRead
);

ULONG
WriteRingBuffer(
	PRING_BUFFER   ringBuffer,
	PUCHAR         writeBuffer,
	ULONG          numberOfBytesToWrite
);

#endif // end DRIVER