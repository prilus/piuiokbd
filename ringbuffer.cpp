#include "common.h"
#include "ringbuffer.h"

PRING_BUFFER
AllocRingBuffer(
	ULONG    Size
)
{
	PRING_BUFFER   ringBuffer = NULL;

	ringBuffer = (PRING_BUFFER)ExAllocatePool(NonPagedPool, sizeof(RING_BUFFER));

	if (!ringBuffer)
		return NULL;

	ringBuffer->buffer = (PUCHAR)ExAllocatePool(NonPagedPool, Size);

	if (!ringBuffer->buffer)
	{
		ExFreePool(ringBuffer);
		return NULL;
	}

	ringBuffer->inPtr = ringBuffer->buffer;
	ringBuffer->outPtr = ringBuffer->buffer;
	ringBuffer->totalSize = Size;
	ringBuffer->currentSize = 0;

	KeInitializeSpinLock(&ringBuffer->spinLock);

	return ringBuffer;
}

VOID
FreeRingBuffer(
	PRING_BUFFER   ringBuffer
)
{
	ExFreePool(ringBuffer->buffer);
	ExFreePool(ringBuffer);
}

ULONG
ReadRingBuffer(
	PRING_BUFFER   ringBuffer,
	PUCHAR         readBuffer,
	ULONG          numberOfBytesToRead
)
/*
   Routine Description:
   This routine reads data from a ring buffer.

   Arguments:
   ringBuffer - pointer to a ring buffer structure
   readBuffer - pointer to a user supplied buffer to transfer data into
   numberOfBytesToRead - number of bytes to read from the ring buffer

   Return Value:
   ULONG - number of bytes read.  May be smaller than requested number of bytes.
*/
{
	ULONG    byteCount;
	KIRQL    oldIrql;

	Ezusb_KdPrint(("ReadRingBuffer() enter\n"));

	if (numberOfBytesToRead > ringBuffer->totalSize)
		return 0;

	if (ringBuffer->currentSize == 0)
		return 0;

	if (numberOfBytesToRead > ringBuffer->currentSize)
		byteCount = ringBuffer->currentSize;
	else
		byteCount = numberOfBytesToRead;

	//
	// two cases.  Read either wraps or it doesn't.
	// Handle the non-wrapped case first
	//
	if ((ringBuffer->outPtr + byteCount - 1) <
		(ringBuffer->buffer + ringBuffer->totalSize))
	{
		Ezusb_KdPrint(("ReadRingBuffer() about to copy a\n"));
		RtlCopyMemory(readBuffer, ringBuffer->outPtr, byteCount);
		ringBuffer->outPtr += byteCount;
		if (ringBuffer->outPtr == ringBuffer->buffer + ringBuffer->totalSize)
			ringBuffer->outPtr = ringBuffer->buffer;
	}
	// now handle the wrapped case
	else
	{
		SIZE_T    fragSize;

		Ezusb_KdPrint(("ReadRingBuffer() about to copy b\n"));
		// get the first half of the read
		fragSize = ringBuffer->buffer + ringBuffer->totalSize - ringBuffer->outPtr;
		RtlCopyMemory(readBuffer, ringBuffer->outPtr, fragSize);

		// now get the rest
		RtlCopyMemory(readBuffer + fragSize, ringBuffer->buffer, byteCount - fragSize);

		ringBuffer->outPtr = ringBuffer->buffer + byteCount - fragSize;
	}

	// 
	// update the current size of the ring buffer.  Use spinlock to insure
	// atomic operation.
	//
	KeAcquireSpinLock(&ringBuffer->spinLock, &oldIrql);
	ringBuffer->currentSize -= byteCount;
	KeReleaseSpinLock(&ringBuffer->spinLock, oldIrql);

	Ezusb_KdPrint(("ReadRingBuffer() exit\n"));

	return byteCount;
}

ULONG
WriteRingBuffer(
	PRING_BUFFER   ringBuffer,
	PUCHAR         writeBuffer,
	ULONG          numberOfBytesToWrite
)
/*
   Routine Description:
   This routine writes data to a ring buffer.  If the requested write size exceeds
   available space in the ring buffer, then the write is rejected.

   Arguments:
   ringBuffer - pointer to a ring buffer structure
   readBuffer - pointer to a user supplied buffer of data to copy to the ring buffer
   numberOfBytesToRead - number of bytes to write to the ring buffer

   Return Value:
   ULONG - number of bytes written.
*/
{
	ULONG    byteCount;
	KIRQL    oldIrql;

	if (numberOfBytesToWrite >
		(ringBuffer->totalSize - ringBuffer->currentSize))
	{
		Ezusb_KdPrint(("WriteRingBuffer() OVERFLOW\n"));
		return 0;
	}

	byteCount = numberOfBytesToWrite;

	//
	// two cases.  Write either wraps or it doesn't.
	// Handle the non-wrapped case first
	//
	if ((ringBuffer->inPtr + byteCount - 1) <
		(ringBuffer->buffer + ringBuffer->totalSize))
	{
		RtlCopyMemory(ringBuffer->inPtr, writeBuffer, byteCount);
		ringBuffer->inPtr += byteCount;
		if (ringBuffer->inPtr == ringBuffer->buffer + ringBuffer->totalSize)
			ringBuffer->inPtr = ringBuffer->buffer;
	}
	// now handle the wrapped case
	else
	{
		SIZE_T    fragSize;

		// write the first fragment
		fragSize = ringBuffer->buffer + ringBuffer->totalSize - ringBuffer->inPtr;
		RtlCopyMemory(ringBuffer->inPtr, writeBuffer, fragSize);

		// now write the rest
		RtlCopyMemory(ringBuffer->buffer, writeBuffer + fragSize, byteCount - fragSize);

		ringBuffer->inPtr = ringBuffer->buffer + byteCount - fragSize;
	}

	// 
	// update the current size of the ring buffer.  Use spinlock to insure
	// atomic operation.
	//
	KeAcquireSpinLock(&ringBuffer->spinLock, &oldIrql);
	ringBuffer->currentSize += byteCount;
	KeReleaseSpinLock(&ringBuffer->spinLock, oldIrql);

	return byteCount;
}
